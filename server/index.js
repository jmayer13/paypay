const path = require('path');
const url = require('url');
const http = require('http');
const cors = require('cors');
const bodyParser = require('body-parser');
const { getEmployees, addEmployee, editEmployee, deleteEmployee } = require('./firebaseClient');

const express = require('express')
const app = express()
const port = 3000

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.get('/employees', function (req, res) {
    getEmployees().then((data)=> {
        res.send(data)
    });
});

app.post('/employee', function (req, res) {
    const data = req.body;
    addEmployee(data).then((data)=> {
        res.send(data)
    });
});

app.put('/employee/:id', function (req, res) {
    const data = req.body;
    let id = req.params.id;
    editEmployee(id, data).then((data)=> {
        res.send(data)
    });
});

app.delete('/employee/:id', function (req, res) {
    let id = req.params.id;
    deleteEmployee(id).then((data)=> {
        res.send(data)
    });
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
