const firebase = require("firebase/app");
require("firebase/auth");
require("firebase/firestore");

const config = require("./config");

const app = firebase.initializeApp(config);
const db = firebase.firestore();

const getEmployees = () => {
    return db.collection("employee").get().then((querySnapshot) => {
        let data = [];

        querySnapshot.forEach((doc) => {
            let employee = doc.data();
            employee.id = doc.id;
            data.push(employee)
        });
        return data;
    });
};

const addEmployee = (data) => {
    return db.collection("employee").add(data)
    .then(function(docRef) {
        data.id = docRef.id
        return data;
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
};

const editEmployee = (id, data) => {
    return db.collection("employee").doc(id).set({
    name: data.name
})
.then(function() {
    return data;
})
.catch(function(error) {
    console.error("Error writing document: ", error);
});
};

const deleteEmployee = (id) => {
    return db.collection("employee").doc(id).delete()
.then(function() {
    return {id};
})
.catch(function(error) {
    console.error("Error writing document: ", error);
});
};

module.exports = {getEmployees, addEmployee, editEmployee, deleteEmployee};
