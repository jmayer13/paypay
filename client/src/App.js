import React from "react";
import {
    BrowserRouter as Router,
    Route,
    Switch,
} from "react-router-dom";
import styled from "styled-components";
import { connect } from "react-redux";
import EmployeeContainer from "./EmployeeContainer";

const Root = styled.div`
    min-height: 100vh;
    padding: 24px;
    margin: auto;
    justify-content: center;
    background-color: #f5f5f5;
`;

const App = () => (
    <Root>
        <Router>
            <Switch>
                <Route
                    path="/admin"
                    component={EmployeeContainer}
                />
            </Switch>
        </Router>
    </Root>
);

const mapStateToProps = state => ({
    ...state,
});
const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
