import { combineReducers } from "redux";
import employees from "./employee/employeeReducers";

const reducers = combineReducers({
    employees,
});

export default reducers;
