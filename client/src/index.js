import React from "react";
import ReactDOM from "react-dom";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import App from "./App";
import api from "./middleware/api";
import reducer from "./reducers";

const store = createStore(
    reducer,
    applyMiddleware(thunk, api),
);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById("root"));
