import axios from "axios";
import qs from "qs";
import { get } from "lodash";
import env from "../config";

export const CALL_API = Symbol("Call API");

export default function api({ dispatch, getState }) {
    return next => (action) => {
        if (!Object.prototype.hasOwnProperty.call(action, CALL_API)) {
            return next(action);
        }

        const callAPI = action[CALL_API];
        const {
            url, headers, method, data, query, types, requireAuth,
        } = callAPI;

        const token = get(getState(), "login.auth.access_token");

        const config = {
            method,
            baseURL: env.apiHost,
            url: url + qs.stringify(query, { addQueryPrefix: true }),
            data: qs.stringify(data),
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                ...(requireAuth ? { Authorization: `Bearer ${token}` } : {}),
                ...headers,
            },
        };

        const [requestType, successType, failureType] = types;
        dispatch({ type: requestType });

        return axios(config).then(
            response => dispatch({ response, type: successType, request: callAPI }),
        ).catch(
            error => dispatch({ error, type: failureType, request: callAPI }),
        );
    };
}
