import React from "react";
import { connect } from "react-redux";
import MaterialTable from "material-table";
import * as action from "./employee/employeeActions";

export class Container extends React.Component {
    componentDidMount() {
        const { fetchEmployees } = this.props;
        fetchEmployees();
    }

    render() {
        const {
            employees,
            onDeleteEmployee,
            onEditEmployee,
            onAddEmployee,
        } = this.props;

        return (
            <MaterialTable
                columns={[
                    { title: "Name", field: "name" },
                ]}
                data={employees}
                title="Employees"
                editable={{
                    onRowAdd: newData => onAddEmployee(newData),
                    onRowUpdate: newData => onEditEmployee(newData),
                    onRowDelete: oldData => onDeleteEmployee(oldData.id),
                }}
            />
        );
    }
}

const mapStateToProps = state => ({
    ...state.employees,
});

const mapDispatchToProps = {
    fetchEmployees: action.fetchEmployees,
    onDeleteEmployee: action.onDeleteEmployee,
    onEditEmployee: action.onEditEmployee,
    onAddEmployee: action.onAddEmployee,
};

export default connect(mapStateToProps, mapDispatchToProps)(Container);
