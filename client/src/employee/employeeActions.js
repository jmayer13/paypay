import { CALL_API } from "../middleware/api";

export const FETCH_EMPLOYEES_REQUEST = "FETCH_EMPLOYEES_REQUEST";
export const FETCH_EMPLOYEES_SUCCESS = "FETCH_EMPLOYEES_SUCCESS";
export const FETCH_EMPLOYEES_FAILURE = "FETCH_EMPLOYEES_FAILURE";
export const DELETE_EMPLOYEE_REQUEST = "DELETE_EMPLOYEE_REQUEST";
export const DELETE_EMPLOYEE_SUCCESS = "DELETE_EMPLOYEE_SUCCESS";
export const DELETE_EMPLOYEE_FAILURE = "DELETE_EMPLOYEE_FAILURE";
export const CLEAR_EMPLOYEES = "CLEAR_EMPLOYEES";
export const EDIT_EMPLOYEE_REQUEST = "EDIT_EMPLOYEE_REQUEST";
export const EDIT_EMPLOYEE_SUCCESS = "EDIT_EMPLOYEE_SUCCESS";
export const EDIT_EMPLOYEE_FAILURE = "EDIT_EMPLOYEE_FAILURE";
export const ADD_EMPLOYEE_REQUEST = "ADD_EMPLOYEE_REQUEST";
export const ADD_EMPLOYEE_SUCCESS = "ADD_EMPLOYEE_SUCCESS";
export const ADD_EMPLOYEE_FAILURE = "ADD_EMPLOYEE_FAILURE";

export const fetchEmployees = () => dispatch => dispatch({
    [CALL_API]: {
        url: "/employees",
        method: "GET",
        types: [
            FETCH_EMPLOYEES_REQUEST,
            FETCH_EMPLOYEES_SUCCESS,
            FETCH_EMPLOYEES_FAILURE,
        ],
    },
});

export const onDeleteEmployee = id => dispatch => dispatch({
    [CALL_API]: {
        url: `/employee/${id}`,
        method: "DELETE",
        types: [
            DELETE_EMPLOYEE_REQUEST,
            DELETE_EMPLOYEE_SUCCESS,
            DELETE_EMPLOYEE_FAILURE,
        ],
    },
});

export const onEditEmployee = employee => dispatch => dispatch({
    [CALL_API]: {
        url: `/employee/${employee.id}`,
        method: "PUT",
        types: [
            EDIT_EMPLOYEE_REQUEST,
            EDIT_EMPLOYEE_SUCCESS,
            EDIT_EMPLOYEE_FAILURE,
        ],
        data: employee,
    },
});

export const onAddEmployee = employee => dispatch => dispatch({
    [CALL_API]: {
        url: "/employee",
        method: "POST",
        types: [
            ADD_EMPLOYEE_REQUEST,
            ADD_EMPLOYEE_SUCCESS,
            ADD_EMPLOYEE_FAILURE,
        ],
        data: employee,
    },
});
