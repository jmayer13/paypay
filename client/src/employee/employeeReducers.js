import createReducer from "../utils/createReducer";
import {
    FETCH_EMPLOYEES_REQUEST,
    FETCH_EMPLOYEES_SUCCESS,
    FETCH_EMPLOYEES_FAILURE,
    DELETE_EMPLOYEE_SUCCESS,
    EDIT_EMPLOYEE_SUCCESS,
    ADD_EMPLOYEE_SUCCESS,
} from "./employeeActions";

export const initialState = {
    employees: [],
    hasMore: false,
    isFetching: false,
};

const handlers = {
    [FETCH_EMPLOYEES_REQUEST](state) {
        return {
            ...state,
            isFetching: true,
        };
    },
    [FETCH_EMPLOYEES_SUCCESS](state, { response }) {
        return {
            ...state,
            employees: response.data,
            isFetching: false,
        };
    },
    [FETCH_EMPLOYEES_FAILURE](state, { error }) {
        return {
            ...state,
            error,
            isFetching: false,
        };
    },
    [ADD_EMPLOYEE_SUCCESS](state, { response }) {
        return {
            ...state,
            employees: state.employees.concat(response.data),
            isFetching: false,
        };
    },
    [EDIT_EMPLOYEE_SUCCESS](state, { response }) {
        return {
            ...state,
            employees: state.employees.map((employee) => {
                if (employee.id === response.data.id) {
                    return response.data;
                }
                return employee;
            }),
            isFetching: false,
        };
    },

    [DELETE_EMPLOYEE_SUCCESS](state, { response }) {
        return {
            ...state,
            employees: state.employees.filter(employee => (employee.id !== response.data.id)),
            isFetching: false,
        };
    },


};

export default createReducer(initialState, handlers);
