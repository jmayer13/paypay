const merge = require('webpack-merge');
const webpack = require("webpack");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = (env) => {
    return merge(common(env), {
        mode: 'production',
        devtool: false,
        optimization: {
            minimizer: [new UglifyJsPlugin()],
            splitChunks: {
                chunks: 'all',
                maxInitialRequests: Infinity,
                minSize: 0,
                cacheGroups: {
                    vendor: {
                        test: /[\\/]node_modules[\\/]/,
                        name(module) {
                            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
                            return `npm.${packageName.replace('@', '')}`;
                        },
                    },
                }
            }
        },
        plugins: [new CompressionPlugin()],
        performance: {
            hints: false,
        }
    });
}
