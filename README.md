# Full Stack Developer Challenge

This is an interview challenges. Please feel free to fork. Pull Requests will be ignored.

## TODO
* [x] setup client
* [x] setup server
* [ ] add tests

### Admin view

* [x] Add/remove/update/view employees
* [ ] Add/update/view performance reviews
* [ ] Assign employees to participate in another employee's performance review

### Employee view

* [ ] List of performance reviews requiring feedback
* [ ] Submit feedback


## Notes

I tried to deliver the first version of an MVP of what would be a product. Having a limited time frame, I focused on the features and took some shortcuts.

Things that I left aside:

- Authentication / Authorization
- unit tests
- other adjustments

I used this project as a practice to learn more about Firestore.
The choices the other technologies were based on the time I had and my expertize with them.


##  Running Locally

Server
```
cd server
npm install
npm start

```
Client
```
cd client
npm install
npm start
```

## Stack 

Client
- React + Redux
- Material-UI


Server
- Node + Express
- Firestore database
